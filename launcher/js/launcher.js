$(function(){
	
	var $menu = $(".gnb>li>a").slice(0,3);
	var $menu_size = 3;
	var $container = $("section>div");	
	
	var $navwrap = $(".navwrap");

	var posContainerTop = new Array();
	posContainerTop.push(0);
	
	$container.each(function(index){
		posContainerTop.push($(this).offset().top);		
	});
	
	
	
	$(window).on("scroll",function(){
		
		var scrollH = $(this).scrollTop();	

		if(scrollH > 696){
			$navwrap.addClass("fixed");
			$(".group_2").css("margin-top","85px");
		}else{
			$navwrap.removeClass("fixed");
			$(".group_2").css("margin-top","0");
		}
		

					
		for(var i=0;i<$menu_size;i++){			
			if(scrollH < posContainerTop[1]){
				$menu.eq(0).parent().addClass("on").siblings().removeClass("on");
				
			}else if(scrollH >= posContainerTop[i+1]-90){
				$menu.eq(i).parent().addClass("on").siblings().removeClass("on");
				
			}else if(i == $menu_size-1 && scrollH > posContainerTop[i+1]-90){
				$menu.eq(i).parent().addClass("on").siblings().removeClass("on");
			}
		}
	});
	
	
	var move = function(topVal){
		$("html,body").stop().animate({
			scrollTop : topVal
		},400,"easeInOutCubic");			
	};

	
	
	
	var $syrup_card_arr = $(".syrup_card_arr");

	$syrup_card_arr.on("click",function(evt){
		move($("article.group_4").offset().top-85);
		evt.preventDefault();
	});
	
	var $theme_arr = $(".theme_arr");

	$theme_arr.on("click",function(evt){
		move($("article.group_6").offset().top-85);
		evt.preventDefault();
	});

	
	$menu.on("click",function(evt){
		var now_idx = $menu.index($(this));

		switch (now_idx){
			case 0:
				move(posContainerTop[now_idx]);
				break;
				
			default:
				move(posContainerTop[now_idx+1]-90);
				break;				
		}		
		evt.preventDefault();
	});

	
});




$(function(){	
	
	
	var $indicator = $(".tip-pagination>li");
	var $comment = $(".comment");
	var $container = $(".tip-container")
	var now_idx = 0;
	var intervalID = "";
	
	var move = function(idx){
		$indicator.eq(idx).addClass("on").siblings().removeClass("on");
		$comment.eq(idx).addClass("on").siblings().removeClass("on");
		$container.stop().animate({
			left:-291*idx
		},200);
	};
	
	$indicator.on({
		mouseenter : function(){
			clearInterval(intervalID);
			now_idx = $indicator.index($(this));
			move(now_idx);		 
		},
		mouseleave : function(){
			auto_start();
		}
	});
	
	
	$container.on({
		mouseenter : function(){
			clearInterval(intervalID);	 
		},
		mouseleave : function(){
			auto_start();
		}
	});

	
	var auto_start = function(){
		intervalID = setInterval(function(){
			if(now_idx>1){
				now_idx = 0;
			}else{
				now_idx++
			}		
			move(now_idx);			
		},3000);
	};
	
	
	$(window).on("load",function(){
		auto_start();
	});
	
})



$(function(){
	

	var card_info = { list : [
		{
			"title":"Syrup table",
			"headline":"Syrup table 카드",
			"explain":"오늘 점심 메뉴로 뭐가 좋을까?",
			"thumb":"card_icon_1.png",
			"viewport":"card_1.png",
			"arrow":"card_arr_1.png",
			"bgc":"rgb(151, 203, 53)"
		},
		{
			"title":"통화",
			"headline":"통화 카드",
			"explain":"다양한 캐릭터와 함께 나만의 <br />멋진 연락처를 간직하세요.",
			"thumb":"card_icon_2.png",
			"viewport":"card_2.png",
			"arrow":"card_arr_2.png",
			"bgc":"rgb(0, 139, 99)"	
		},
		{
			"title":"날씨",
			"headline":"웨더 카드",
			"explain":"정확한 날씨 정보와 함께 <br />감성까지..",
			"thumb":"card_icon_3.png",
			"viewport":"card_3.png",
			"arrow":"card_arr_3.png",
			"bgc":"rgb(54, 69, 91)"	
		},
		{
			"title":"트위터",
			"headline":"트위터 카드",
			"explain":"트위터를 빠르게! <br />그리고 더 재미있게!",
			"thumb":"card_icon_4.png",
			"viewport":"card_4.png",
			"arrow":"card_arr_4.png",
			"bgc":"rgb(72, 149, 207)"	
		},
		{
			"title":"페이스북",
			"headline":"페이스북 카드",
			"explain":"페이스북의 새로운 경험. 멋진 스타일과 <br />편리한 UI를 체험해 보세요",
			"thumb":"card_icon_5.png",
			"viewport":"card_5.png",
			"arrow":"card_arr_5.png",
			"bgc":"rgb(62, 83, 119)"	
		}
		,
		{
			"title":"쇼킹딜 11am",
			"headline":"쇼킹딜 11am 카드",
			"explain":"쇼핑! 더 편리하고, 파격적인<br />할인을 원하신다면…",
			"thumb":"card_icon_6.png",
			"viewport":"card_6.png",
			"arrow":"card_arr_6.png",
			"bgc":"rgb(240, 140, 64)"	
		},
		{
			"title":"검색",
			"headline":"검색 카드",
			"explain":"검색은 서비스카드로<br />한번에!",
			"thumb":"card_icon_7.png",
			"viewport":"card_7.png",
			"arrow":"card_arr_7.png",
			"bgc":"rgb(10, 186, 134)"	
		},
		{
			"title":"알람",
			"headline":"알람/시계 카드",
			"explain":"쉽고 간편하게 설정하는<br />나만의 알람",
			"thumb":"card_icon_8.png",
			"viewport":"card_8.png",
			"arrow":"card_arr_8.png",
			"bgc":"rgb(66, 153, 190)"	
		},
		{
			"title":"웹툰",
			"headline":"웹툰 카드",
			"explain":"웹툰도 서비스카드로 <br />스마트하게 즐겨라!",
			"thumb":"card_icon_9.png",
			"viewport":"card_9.png",
			"arrow":"card_arr_9.png",
			"bgc":"rgb(234, 174, 17)"	
		},
		{
			"title":"설정",
			"headline":"설정 카드",
			"explain":"자주 사용하는스마트폰 설정을<br />손쉽게 사용해보세요",
			"thumb":"card_icon_10.png",
			"viewport":"card_10.png",
			"arrow":"card_arr_10.png",
			"bgc":"rgb(92, 177, 114)"	
		},
		{
			"title":"세계날씨",
			"headline":"세계날씨 카드",
			"explain":"아름다운 사진과 함께<br />날씨를 확인하세요.",
			"thumb":"card_icon_11.png",
			"viewport":"card_11.png",
			"arrow":"card_arr_11.png",
			"bgc":"rgb(130, 80, 176)"	
		},
		{
			"title":"인스타그램",
			"headline":"Instagram 카드",
			"explain":"인스타그램을 더 나은<br />기능으로 사용해보세요",
			"thumb":"card_icon_12.png",
			"viewport":"card_12.png",
			"arrow":"card_arr_12.png",
			"bgc":"rgb(39, 79, 113)"	
		},
		{
			"title":"맵",
			"headline":"Maps 카드",
			"explain":"내 주변 핫플레이스<br />서비스 카드로 확인하라",
			"thumb":"card_icon_13.png",
			"viewport":"card_13.png",
			"arrow":"card_arr_13.png",
			"bgc":"rgb(55, 64, 181)"	
		},
		{
			"title":"T map",
			"headline":"T map 카드",
			"explain":"가장 빠른 길 찾기! T map!",
			"thumb":"card_icon_14.png",
			"viewport":"card_14.png",
			"arrow":"card_arr_14.png",
			"bgc":"rgb(203, 42, 45)"	
		},
		{
			"title":"포토앨범",
			"headline":"포토앨범 카드",
			"explain":"나만의 포토 갤러리를<br />쉽게 관리",
			"thumb":"card_icon_15.png",
			"viewport":"card_15.png",
			"arrow":"card_arr_15.png",
			"bgc":"rgb(232, 187, 27)"	
		},
		{
			"title":"Maxim 카드",
			"headline":"Maxim 카드",
			"explain":"지구 최강 남성지 MAXIM의<br /> 강렬한 화보와 뉴스",
			"thumb":"card_icon_16.png",
			"viewport":"card_16.png",
			"arrow":"card_arr_16.png",
			"bgc":"rgb(202, 38, 41)"	
		},
		{
			"title":"일정",
			"headline":"일정 카드",
			"explain":"다양한 계정의 일정을<br />한눈에 확인해보세요",
			"thumb":"card_icon_17.png",
			"viewport":"card_17.png",
			"arrow":"card_arr_17.png",
			"bgc":"rgb(225, 135, 115)"	
		},
		{
			"title":"허핑턴포스트",
			"headline":"허핑턴포스트 카드",
			"explain":"전 세계 칼럼리스트가<br />전하는 생생한 이슈",
			"thumb":"card_icon_18.png",
			"viewport":"card_18.png",
			"arrow":"card_arr_18.png",
			"bgc":"rgb(36, 94, 81)"	
		},
		{
			"title":"Youtube",
			"headline":"Youtube 카드",
			"explain":"유튜브 인기동영상을 서비스카드 <br />한 장으로 즐긴다!",
			"thumb":"card_icon_19.png",
			"viewport":"card_19.png",
			"arrow":"card_arr_19.png",
			"bgc":"rgb(143, 19, 11)"	
		},
		{
			"title":"T클라우드",
			"headline":"T cloud 카드",
			"explain":"T cloud의 기능을<br />서비스카드로 사용해보세요",
			"thumb":"card_icon_20.png",
			"viewport":"card_20.png",
			"arrow":"card_arr_20.png",
			"bgc":"rgb(74, 138, 202)"	
		},
		{
			"title":"캐시슬라이드",
			"headline":"캐시슬라이드 카드",
			"explain":"첫 화면에 혜택을 담다!",
			"thumb":"card_icon_21.png",
			"viewport":"card_21.png",
			"arrow":"card_arr_21.png",
			"bgc":"rgb(225, 199, 1)"	
		},
		{
			"title":"Syrup",
			"headline":"Syrup 카드",
			"explain":"생활에 달콤함을 더하다!",
			"thumb":"card_icon_22.png",
			"viewport":"card_22.png",
			"arrow":"card_arr_22.png",
			"bgc":"rgb(236, 124, 71)"	
		},
		{
			"title":"OK캐쉬백",
			"headline":"OK캐쉬백 카드",
			"explain":"포인트를 즐겨라! <br />OK캐쉬백을 간편하게!",
			"thumb":"card_icon_23.png",
			"viewport":"card_23.png",
			"arrow":"card_arr_23.png",
			"bgc":"rgb(209, 70, 72)"	
		},
		{
			"title":"헬스온",
			"headline":"헬스온 카드",
			"explain":"당신의 건강을 위해<br />가장 먼저 해야 할일!",
			"thumb":"card_icon_24.png",
			"viewport":"card_24.png",
			"arrow":"card_arr_24.png",
			"bgc":"rgb(72, 159, 218)"	
		}
	]};
	

	
	var $cardlist = $("#srvcard .cardlist");

	$.each(card_info.list,function(idx,value){		
		var child = 
			'<li><a href="#"><img src="images/card/' +
			$(this)[0].thumb +
			'" alt="#" /><span class="title">' +
			this.title+'</span></a></li>';
		
		$cardlist.append(child);				
	});
	
	
	
	$cardlist.on("click","li>a",function(evt){
		
		var idx = $cardlist.find("a").index($(this));
		
		$(".viewport").attr({
			src:"images/card/"+card_info.list[idx].viewport
		});
		
		$(".headline").text(card_info.list[idx].headline);
		
		$(".explain").html(card_info.list[idx].explain);
		
		$(".syrup_card_arr").css({
			"background-image":'url(images/card/'+card_info.list[idx].arrow+')'
		});
		
		$("#srvcard>.group_4").css({
			"background-color":card_info.list[idx].bgc
		});
		
		evt.preventDefault();
	});
});










