var oldSlide = null; 	
var oldIdx = -1;		
var selectSlideNo = 0;	
var pagenationLen;		
var motionFlag = false;


var addLeftClone = function(){
	for(var i = 0; i < 13; i++)
	{
		var element = "<li class='leftClone'><a href='javascript:changeSlide(" + -(i + 1) + 
		");'><img src='images/slide/theme_thumb" +
		(pagenationLen - i) + "_n.png' /></a></li>";
		
		$(".slides-pagination").append("<li class='leftClone'><a href='javascript:changeSlide("+ -(i + 1) +");'><img src='images/slide/theme_thumb"+(pagenationLen - i)+"_n.png' /></a></li>");
		$(".slides-pagination").find(".leftClone").each( function ( index )
		{
			$(this).css({left:(-134 * (index+1))-57});
			$(this).attr("data-pos", (-134 * (index+1))-57);
			$(this).attr("data-idx", -(index + 1) );
		});		
	}
}


var addRightClone = function(){
	for(var i = 0; i < 13; i++)
	{
		$(".slides-pagination").append("<li class='rightClone'><a href='javascript:changeSlide("+ ( i + pagenationLen ) +");'><img src='images/slide/theme_thumb"+(i+1)+"_n.png' /></a></li>");
		$(".slides-pagination").find(".rightClone").each( function ( index )
		{
			$(this).css({left:(134 * (index+pagenationLen))-57});
			$(this).attr("data-pos", (134 * (index+pagenationLen))-57);
			$(this).attr("data-idx", (index + pagenationLen) );
		});		
	}
}


var changeSlide = function mySelfChange(idx,motion){

	if(motionFlag) return;

	var duration = 400;
	var idx2 = idx;

	if(motion){
		duration = 0;
	}	

	if(idx < pagenationLen && idx >= 0){
		$(".slides-pagination").stop().animate({left:-134*idx}, duration, "easeInOutCubic", function (){
			motionFlag = false;
		});
	}else{
		$(".slides-pagination").stop().animate({left:-134*idx}, duration, "easeInOutCubic", function(){
			motionFlag = false;
			if(idx2 > pagenationLen-1 || idx2 < 0){				
				mySelfChange(idx, true);
			}			
		});
	}

	$(".slides-pagination").find("li").each(function(i){
		if(parseInt($(this).attr("data-idx")) == idx){
			$(this).stop().animate({left:parseInt($(this).attr("data-pos"))}, {duration:duration, easing:"easeInOutCubic"});
		}else if(parseInt($(this).attr("data-idx")) > idx){
			$(this).stop().animate({left:parseInt($(this).attr("data-pos"))+90}, {duration:duration, easing:"easeInOutCubic"});
		}else{
			$(this).stop().animate({left:parseInt($(this).attr("data-pos"))-90}, {duration:duration, easing:"easeInOutCubic"});
		}
	});

	
	if(motion){
		return;
	}


	if(idx > pagenationLen-1){
		idx = idx - pagenationLen;
	}	
	
	if(idx < 0){
		idx = pagenationLen + idx;
	}				
	
	var target = $(".slides-control").find("li").eq(idx);
	
	if(idx2 > oldIdx){
		oldSlide.stop().animate({left:-243}, {duration:duration, easing:"easeInOutCubic"});
		target.stop().show().css({left:243}).animate({left:0}, {duration:duration, easing:"easeInOutCubic"});
	}else{
		oldSlide.stop().animate({left:243}, {duration:duration, easing:"easeInOutCubic"});
		target.stop().show().css({left:-243}).animate({left:0}, {duration:duration, easing:"easeInOutCubic"});
	}
	
	

	var slideTitle = new Array();
	var themeSort = new Array();
	$(".slides-container img").attr({
		alt : function(index,value){
				slideTitle.push(value);
			},
		"data-themesort" : function(index,value){
				themeSort.push(value);
			}
	});	

	$("#theme .title").text(slideTitle[idx]);
	$("#theme .themesort").css({
		"background-image":'url(images/ico_theme'+themeSort[idx]+'.gif)'
	});


	selectSlideNo = idx;	
	oldSlide = target;
	oldIdx = idx;
	motionFlag = true;

}


var auto_start = function(){
	intervalID = setInterval(function(){
		selectSlideNo++;
		changeSlide(selectSlideNo);		
	},3000);
}


var auto_stop = function(){
	clearInterval( intervalID );
}


$(function (){
	pagenationLen = $(".slides-pagination").find("li").length;

	$(".slides-control").find("li").hide();

	$(".slides-pagination").find("li").each(function(index){
		$(this).css({left:(134*index)-57});
		$(this).attr("data-pos", (134*index)-57);
		$(this).attr("data-idx", index);
	});

	addLeftClone();
	addRightClone();

	oldSlide = $(".slides-control").find("li").eq(0);
	oldSlide.show();
	changeSlide(0, true);

	$(".slides-pagination").on("mouseover", auto_stop);
	$(".slides-pagination").on("mouseout", auto_start);
	auto_start();

});