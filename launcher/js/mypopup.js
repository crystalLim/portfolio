$.fn.mypopup = function(option){

	var className = $(this)[0].className;
	var popID = "mypopup_" + className;
	

	var element = 	'<aside id="'+popID+'">\
						<div class="popup_container">\
							<div class="popup">\
								<a href="#" class="clse_popup">닫기</a>\
							</div>\
						</div>\
					</aside>';
	
	$("body").append(element);
	
	var $popup_cont = $("."+className+"_cont");
	
	
	var settings = $.extend({
		width : 524,
		height : 357,
		marginTop : 160,
		backgroundImage : "bg_ready.png",
		zIndex : 100
	},option||{});
	$popup_cont.css(settings);
	$('#'+popID+' .popup').append($popup_cont);


	var container = '#'+popID+'>.popup_container';
	

	$(this).on('click',function(evt){
		$(container).css({
				"z-index" : settings.zIndex
			}).find(".popup").css({
				"width" : settings.width,
				"height" : settings.heigth,
				"margin-top" : settings.marginTop
			}).children("."+className+"_cont").css({
				"background-image" : "url(images/"+settings.backgroundImage+")",
				"background-repeat":"no-repeat",
				"background-size":"cover"
			});		
		
		$(container).addClass('on');
		evt.preventDefault();		
	});
	
	

	$(container).on('click', function(evt){
		if( $(event.target,this).is('#'+popID+' .clse_popup') || $(event.target).is(container) ) {	
			$(this).removeClass('on');			
		}		
		evt.preventDefault();
	});
	
	

	$(document).on("keyup",function(evt){
    	if(evt.which=='27'){
    		$(container).removeClass('on');
	    }
    });
};