$(function(){

  /* btn */
  var $reservations_btn = $(".btn_area button");
  $reservations_btn.on("click", function(){
    $(this).addClass("on").siblings().removeClass("on");
  });

  $(".sub_select").on("click", function(evt){
    $(this).find(".sub_select_list").toggle();

    evt.preventDefault();
  });

  $(".sub_select_list").on("mouseleave", function(){
     $(this).hide();
  });

  /* 클릭한것 값 적용 */
   $(".sub_select_list_1").find("a").on("click", function(evt){
     $(".sub_select #adult").val($(this).text());
     evt.preventDefault();
   });
   $(".sub_select_list_2").find("a").on("click", function(evt){
     $(".sub_select #child").val($(this).text());
     evt.preventDefault();
   });
   $(".sub_select_list_3").find("a").on("click", function(evt){
     $(".sub_select #infant").val($(this).text());
     evt.preventDefault();
   });

});
