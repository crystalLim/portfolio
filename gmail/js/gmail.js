$(function(){
	var now_idx, old_idx;
	var $title = $("#help-container>.menu>li>a");
	var $tab = $("#help-container>.menu>li>.tab");
	
	$(window).on("load",function(){
		$title.data("flag",false);	// 내부 데이터 저장 기법 활용
	});//최초 로드시 실행코드
	
	$title.on("click",function(evt){
		now_idx = $title.index($(this));
		var flagVal = $(this).data("flag");
		
		if(!flagVal){//닫혀있는 상태의 제목을 클릭한 경우
			
			$(this).next().slideDown(function(){
				$(".tab_"+now_idx).stop().animate({bottom:0},100);
			}).parent().addClass("active").siblings().removeClass("active").children().next(".sub").slideUp(function(){
				$(".tab").not(".tab_"+now_idx).stop().animate({bottom:-260},100);
			});
			
			$title.data("flag",false);
			$(this).data("flag",true);
			
			old_idx = now_idx;
			
		}else if(old_idx == now_idx){	// 이미 열려있는 제목을 다시 클릭한 경우
				$(this).parent().removeClass("active").children().next(".sub").slideUp(function(){
					$(".tab_"+now_idx).stop().animate({bottom:-260})
				});
				
				$(this).data("flag",false);


		}
		evt.preventDefault();
	});
});