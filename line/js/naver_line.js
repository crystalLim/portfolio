$(function(){
		$('#mainbanr').slidesjs({
			width: 960, // 실제 이미지의 비율에 따른 사이즈를
			height: 440, // 액자의 높이를 기준으로 비례식 계산하여 입력
			start:1,
			navigation: {
				active:true, 	// true 선택시 이전/다음 버튼 생성
				effect:"fade"	// "slide" 또는 "fade"
			},
			
			pagination:{
				active:true,	// 페이지네이션 생성 - 만약 false 하면 트리거를 걸어도 작동 X
				effect:"fade"	// 인디케이터를 눌렀을 때 "slide" or "fade".
			},
			
			play:{
				effect:"fade",		// "slide" 또는 "fade"
				auto:true,			// 페이지 시작시 자동시작 설정
				active: true,		// 시작중지 버튼 보이기
				swap: false,		// true 시작중지 버튼 하나로	
				interval:4000,		// 1000분의 1초 단위 시간간격				
				pauseOnHover:true,	// 이미지에 hover시 멈춤기능
				restartDelay:4000	// 멈췄을 때 다시 시작되는 시간
			},
			
			callback: {
				loaded: function(){
				  // 시작,정지버튼 숨김처리
				  $('.slidesjs-play').hide(0);			  
				  $('.slidesjs-stop').hide(0);			  
				}
			}
		});

		$('#mainbanr>.slidesjs-previous').text("이전");
		$('#mainbanr>.slidesjs-next').text("다음");
});//슬라이드 제이에스 관련


$(function(){
	var $langlist = $(".langlist");
	
	$(".opt input#lang, .opt .arrow").on("click",function(){
		$langlist.toggle();
	});//1. 언어선택 옵션리스트 노출
	
	$langlist.on("mouseleave",function(){
		$(this).hide();
	});//2. 전체목록 영역을 벗어날 경우 숨김처리
	
	$langlist.find("a").on("click",function(evt){
		$(".opt input#lang").val($(this).text());//언어선택시 입력상자내용 변경		
		$(this).parent().addClass("on").siblings().removeClass("on");//목록에서 색상표시		
		$langlist.toggle();//전체목록의 숨김처리		
		evt.preventDefault();
	});//3. 언어선택에 대한 클릭이벤트 설정
	
});// 언어 선택 옵션 박스에 대한 설정


$(function(){
	
	var $topmenu = $(".gnb>li>a");
	var topmenu_size = $topmenu.length;
	
	//1단계 - 배열에 컨텐츠 y좌표값을 추가
	
	var arrContH = new Array();	
	arrContH.push($("#mainbanr").offset().top);// 1) 메인배너의 높이값 추가
	
	$("#mainsrv article").each(function(idx){
		arrContH.push($(this).offset().top);
	});// 2) article이 시작되는 y좌표값을 동적으로 배열에 저장, 이렇게 하면 나중에 높이가 
	// 변경되더라도 정확하게 시작점으로 스크롤이 가능하다.
	
	console.log(arrContH);
	
	$(window).on("scroll",function(){
		/*		
			키포인트1 - 컨텐츠의 높이를 저장한는 배열에 최상단의 기본 메인배너도 포함
			키포인트2 - for문을 이용한 i값의 비교 기준을 탑메뉴의 개수로 설정
			키포인트3 - 스크롤 높이값에 따른 메인메뉴 롤오버 설정을 3가지 경우로 나눠서 작성
		*/		
		var scrollH = $(this).scrollTop();//현재 스크롤 높이값
		
		for(var i=0;i<topmenu_size;i++){
			if(scrollH<arrContH[1]-400){
				$topmenu.parent().removeClass("on");
			}else if(scrollH>arrContH[i+1]-400){
				$topmenu.eq(i).parent().addClass("on").siblings().removeClass("on");
				//두번째부터 마지막 전까지의 컨텐츠 article 
			}
		}		
	});// 2단계) 스크롤 높이값에 따른 메뉴의 롤오버 색상 변화 - 3가지 경우의 수
	
	
	$topmenu.on("click", function(evt){
		var now_idx = $topmenu.index($(this));
		$("html,body").stop().animate({
			scrollTop : arrContH[now_idx+1]-150
		},400,"easeInOutCubic");
		evt.preventDefault();
	});//3단계) 메인메뉴에 상응하는 컨텐츠 영역으로 애니메이트
	
	
	$(".logo").on("click",function(evt){
		$("html,body").stop().animate({
			scrollTop : 0
		},400,"easeInOutCubic");		
		evt.preventDefault();
	});//4단계) 로고를 클릭하면 컨텐츠영역 상단으로 애니메이트
	
});//scroll과 scrollTop()을 이용한 원페이지 구현


$(function(){
	
	var $aside = $("aside");
	
	$("#mainbanr>.slidesjs-container a").on("click",function(evt){
		$aside.show();
		$("#email").val("");
		$("#pwd").val("");		
		evt.preventDefault();
	});//메인배너 a 클릭이벤트 설정
	
	$("aside .clse, aside>.shadow").on("click",function(){
		$aside.hide();
	});
	
	$(document).on("keyup",function(evt){
		if(evt.which == "27"){
			$aside.hide();
		}
	});//ESC 키에 대한 이벤트 설정
	
	
	$("aside>.shadow>.login_form").on("click",function(evt){
		evt.stopPropagation();
	});//전체 그림자 영역에서 로그인 박스 부분에 대한 클릭 이벤트 전파를 차단
	
	
	$("aside .help").on("click",function(){
		$(this).next(".popup").toggle();
	});//물음표 버튼에 대한 클릭이벤트 설정
	
	
	$("input#email, input#pwd").on("keyup",function(){
		if($("#email").val() != "" && $("#pwd").val() != ""){
			$("aside .btn_login").addClass("on");
		}else{
			$("aside .btn_login").removeClass("on");
		}
	});//input 박스 2개에 내용이 입력됐을 경우 로그인 버튼 활성화
	
});//로그인 라이트박스













